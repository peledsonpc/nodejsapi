var express = require("express");
var cors = require('cors');
var app = express();

app.use(cors())
app.get("/url", (req, res, next) => {
    res.json(["Tony","Lisa","Michael","Ginger","Food"]);
   });


app.listen(3000, () => {
 console.log("Server running on port 3000");
});